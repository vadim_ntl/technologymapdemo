insert into AREA (area_name) values ('Kitchen');
insert into AREA (area_name) values ('Zal');
insert into AREA (area_name) values ('Tualet');
insert into AREA (area_name) values ('Vanna');

insert into PURPOSE (purpose_name) values ('Rukami');
insert into PURPOSE (purpose_name) values ('Moi pol');
insert into PURPOSE (purpose_name) values ('Dezenfek');
insert into PURPOSE (purpose_name) values ('Obezhir');
insert into PURPOSE (purpose_name) values ('Otbel');
insert into PURPOSE (purpose_name) values ('Ruchkami2');
insert into PURPOSE (purpose_name) values ('Ruchkami3');
insert into PURPOSE (purpose_name) values ('Ruchkami4');
insert into PURPOSE (purpose_name) values ('Ruchkami5');
insert into PURPOSE (purpose_name) values ('Ruchkami6');
insert into PURPOSE (purpose_name) values ('Ruchkami7');
insert into PURPOSE (purpose_name) values ('Ruchkami8');
insert into PURPOSE (purpose_name) values ('Ruchkami9');
insert into PURPOSE (purpose_name) values ('Ruchkami10');

INSERT INTO DETERGENT (detergent_short_name, detergent_name) VALUES ('Vanish', 'Vanish Vanish Vanish Vanish ');
INSERT INTO DETERGENT (detergent_short_name, detergent_name) VALUES ('Comet', 'Comet Comet Comet Comet Comet');
INSERT INTO DETERGENT (detergent_short_name, detergent_name) VALUES ('Pemolux', 'Pemolux Pemolux Pemolux Pemolux ');
INSERT INTO DETERGENT (detergent_short_name, detergent_name) VALUES ('Fairy', 'Fairy Fairy Fairy Fairy Fairy Fairy');
INSERT INTO DETERGENT (detergent_short_name, detergent_name) VALUES ('CocaCola', 'CocaCola CocaCola CocaCola');

INSERT INTO DOSAGE (dosage_content) VALUES ('nalit ochen mnogo');
INSERT INTO DOSAGE (dosage_content) VALUES ('pol vedra');
INSERT INTO DOSAGE (dosage_content) VALUES ('do verha');
INSERT INTO DOSAGE (dosage_content) VALUES ('stakan');

INSERT INTO AREA_PURPOSE_RELATION (area_id, purpose_id) VALUES (1, 1);
INSERT INTO AREA_PURPOSE_RELATION (area_id, purpose_id) VALUES (1, 2);
INSERT INTO AREA_PURPOSE_RELATION (area_id, purpose_id) VALUES (1, 3);
INSERT INTO AREA_PURPOSE_RELATION (area_id, purpose_id) VALUES (2, 1);
INSERT INTO AREA_PURPOSE_RELATION (area_id, purpose_id) VALUES (2, 4);

INSERT INTO PURPOSE_DETERGENT_RELATION (purpose_id, detergent_id) VALUES (1, 1);
INSERT INTO PURPOSE_DETERGENT_RELATION (purpose_id, detergent_id) VALUES (1, 2);
INSERT INTO PURPOSE_DETERGENT_RELATION (purpose_id, detergent_id) VALUES (2, 1);
INSERT INTO PURPOSE_DETERGENT_RELATION (purpose_id, detergent_id) VALUES (3, 2);
INSERT INTO PURPOSE_DETERGENT_RELATION (purpose_id, detergent_id) VALUES (3, 4);

INSERT INTO DETERGENT_DOSAGE_RELATION (detergent_id, dosage_id) VALUES (1, 1);
INSERT INTO DETERGENT_DOSAGE_RELATION (detergent_id, dosage_id) VALUES (1, 2);
INSERT INTO DETERGENT_DOSAGE_RELATION (detergent_id, dosage_id) VALUES (2, 2);
INSERT INTO DETERGENT_DOSAGE_RELATION (detergent_id, dosage_id) VALUES (2, 3);

INSERT INTO PERIODICITY (periodicity_name) VALUES ('raz v chas');
INSERT INTO PERIODICITY (periodicity_name) VALUES ('raz v den');
INSERT INTO PERIODICITY (periodicity_name) VALUES ('raz v mecaz');
INSERT INTO PERIODICITY (periodicity_name) VALUES ('raz v kvartal');
INSERT INTO PERIODICITY (periodicity_name) VALUES ('raz v god');

INSERT INTO TEMPERATURE (temperature_value) VALUES ('0-10');
INSERT INTO TEMPERATURE (temperature_value) VALUES ('0-30');
INSERT INTO TEMPERATURE (temperature_value) VALUES ('50-70');
INSERT INTO TEMPERATURE (temperature_value) VALUES ('700-1000');

INSERT INTO EXPOTIME (expotime_value) VALUES ('1 min');
INSERT INTO EXPOTIME (expotime_value) VALUES ('10-15 min');
INSERT INTO EXPOTIME (expotime_value) VALUES ('2 hours');

INSERT INTO SIZ (siz_name) VALUES ('perhcatki');
INSERT INTO SIZ (siz_name) VALUES ('sapogi');
INSERT INTO SIZ (siz_name) VALUES ('bronik');

INSERT INTO USAGES (usage_name, usage_standard) VALUES ('pipec1', 'Standard');
INSERT INTO USAGES (usage_name, usage_standard) VALUES ('pipec2', 'Standard');
INSERT INTO USAGES (usage_name, usage_standard) VALUES ('pipec3', 'Nonstandard');

INSERT INTO AREACOLOR (areacolor_name, areacolor_value) VALUES ('red', '#FF0000');
INSERT INTO AREACOLOR (areacolor_name, areacolor_value) VALUES ('yellow', '#FFFF00');
INSERT INTO AREACOLOR (areacolor_name, areacolor_value) VALUES ('lime', '#008000');
INSERT INTO AREACOLOR (areacolor_name, areacolor_value) VALUES ('blue', '#0000FF');

INSERT INTO TECHNOLOGYMAP (areacolor1_id, areacolor2_id, area_id, purpose_id, detergent_id,
                          dosage_id, periodicity_id, temperature_id, expotime_id, siz_id) VALUES (1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

INSERT INTO TECHNOLOGYMAP_USAGE_RELATION(technologymap_id, usage_id, orderno) VALUES (1, 1, 1);
INSERT INTO TECHNOLOGYMAP_USAGE_RELATION(technologymap_id, usage_id, orderno) VALUES (1, 2, 2);