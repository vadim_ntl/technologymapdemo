package ru.netlly.technologymap.service;

import ru.netlly.technologymap.domain.AreaColor;

import java.util.List;

public interface AreaColorService {
    public List<AreaColor> findAll();

    public AreaColor findById(Long id);

    public AreaColor save(AreaColor areaColor);

    public void deleteById(Long id);
}
