package ru.netlly.technologymap.service;

import ru.netlly.technologymap.domain.Temperature;

import java.util.List;

public interface TemperatureService {
    public List<Temperature> findAll();

    public Temperature findById(Long id);

    public Temperature save(Temperature temperature);

    public void deleteById(Long id);
}
