package ru.netlly.technologymap.service;

import ru.netlly.technologymap.domain.Detergent;

import java.util.List;

public interface DetergentService {
    public List<Detergent> findAll();

    public Detergent findById(Long id);

    public Detergent findByDetergentName(String detergentName);

    public Detergent findByIdWithRelation(Long id);

    public Detergent save(Detergent detergent);

    public void deleteById(Long id);
}

