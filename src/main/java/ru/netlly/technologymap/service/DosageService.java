package ru.netlly.technologymap.service;

import ru.netlly.technologymap.domain.Dosage;

import java.util.List;

public interface DosageService {
    public List<Dosage> findAll();

    public Dosage findById(Long id);

    public Dosage save(Dosage dosage);

    public void deleteById(Long id);
}
