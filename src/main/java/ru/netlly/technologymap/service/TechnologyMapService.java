package ru.netlly.technologymap.service;

import ru.netlly.technologymap.domain.TechnologyMap;

import java.util.List;

public interface TechnologyMapService {
    public List<TechnologyMap> findAll();

    public TechnologyMap findById(Long id);

    public TechnologyMap save(TechnologyMap technologyMap);

    public void deleteById(Long id);
}
