package ru.netlly.technologymap.service;

import ru.netlly.technologymap.domain.Periodicity;

import java.util.List;

public interface PeriodicityService {
    public List<Periodicity> findAll();

    public Periodicity findById(Long id);

    public Periodicity save(Periodicity periodicity);

    public void deleteById(Long id);
}
