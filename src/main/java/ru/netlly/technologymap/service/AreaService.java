package ru.netlly.technologymap.service;

import ru.netlly.technologymap.domain.Area;

import java.util.List;

public interface AreaService {

    public List<Area> findAll();

    public Area findById(Long id);

    public Area findByIdWithRelation(Long id);

    public Area save(Area area);

    public void deleteById(Long id);
}
