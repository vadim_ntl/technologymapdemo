package ru.netlly.technologymap.service;

import ru.netlly.technologymap.domain.Usage;

import java.util.List;

public interface UsageService {
    public List<Usage> findAll();

    public List<Usage> findStandardAll();

    public List<Usage> findNonstandardAll();

    public Usage findById(Long id);

    public Usage save(Usage usage);

    public void deleteById(Long id);
}
