package ru.netlly.technologymap.service;

import ru.netlly.technologymap.domain.ExpoTime;

import java.util.List;

public interface ExpoTimeService {
    public List<ExpoTime> findAll();

    public ExpoTime findById(Long id);

    public ExpoTime save(ExpoTime expoTime);

    public void deleteById(Long id);
}
