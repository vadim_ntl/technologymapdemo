package ru.netlly.technologymap.service.jpa;

import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.netlly.technologymap.domain.Detergent;
import ru.netlly.technologymap.repository.DetergentRepository;
import ru.netlly.technologymap.service.DetergentService;

import java.util.List;


@Service("detergentService")
@Repository
@Transactional
public class DetergentServiceImpl implements DetergentService {

    final Logger logger = LoggerFactory.getLogger(DetergentService.class);

    @Autowired
    private DetergentRepository detergentRepository;

    @Transactional(readOnly = true)
    public List<Detergent> findAll() {
        return Lists.newArrayList(detergentRepository.findAll());
    }

    @Transactional(readOnly = true)
    public Detergent findById(Long id) {
        return detergentRepository.findOne(id);
    }

    @Transactional(readOnly = true)
    public Detergent findByDetergentName(String detergentName) {
        return detergentRepository.findByDetergentName(detergentName);
    };

    public Detergent findByIdWithRelation(Long id) {
        return detergentRepository.findByIdWithRelation(id);
    }

    public void deleteById(Long id) {
        detergentRepository.delete(id);
    }

    public Detergent save(Detergent detergent) {
        return detergentRepository.save(detergent);
    }

}
