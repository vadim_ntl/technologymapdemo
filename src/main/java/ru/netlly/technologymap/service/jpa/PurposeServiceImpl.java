package ru.netlly.technologymap.service.jpa;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.netlly.technologymap.domain.Purpose;
import ru.netlly.technologymap.repository.PurposeRepository;
import ru.netlly.technologymap.service.PurposeService;

import java.util.List;


@Service("purposeService")
@Repository
@Transactional
public class PurposeServiceImpl implements PurposeService {

    @Autowired
    private PurposeRepository purposeRepository;

    public List<Purpose> findAll() {
        return Lists.newArrayList(purposeRepository.findAll());
    }

    public Purpose findById(Long id) {
        return purposeRepository.findOne(id);
    }

    public Purpose findByIdWithRelation(Long id) {
        return purposeRepository.findByIdWithRelation(id);
    }

    public Purpose save(Purpose purpose) {
        return purposeRepository.save(purpose);
    }

    public void deleteById(Long id) {
        purposeRepository.delete(id);
    }
}
