package ru.netlly.technologymap.service.jpa;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.netlly.technologymap.domain.AreaColor;
import ru.netlly.technologymap.repository.AreaColorRepository;
import ru.netlly.technologymap.service.AreaColorService;

import java.util.List;

@Service("areaColorService")
@Repository
@Transactional
public class AreaColorServiceImpl implements AreaColorService {

    @Autowired
    private AreaColorRepository areaColorRepository;

    @Override
    public List<AreaColor> findAll() {
        return Lists.newArrayList(areaColorRepository.findAll());
    }

    @Override
    public AreaColor findById(Long id) {
        return areaColorRepository.findOne(id);
    }

    @Override
    public AreaColor save(AreaColor areaColor) {
        return areaColorRepository.save(areaColor);
    }

    @Override
    public void deleteById(Long id) {
        areaColorRepository.delete(id);
    }
}
