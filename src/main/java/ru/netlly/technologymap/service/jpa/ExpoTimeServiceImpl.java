package ru.netlly.technologymap.service.jpa;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.netlly.technologymap.domain.ExpoTime;
import ru.netlly.technologymap.repository.ExpoTimeRepository;
import ru.netlly.technologymap.service.ExpoTimeService;

import java.util.List;

@Service("expoTimeService")
@Repository
@Transactional
public class ExpoTimeServiceImpl implements ExpoTimeService{

    @Autowired
    private ExpoTimeRepository expoTimeRepository;

    @Override
    public List<ExpoTime> findAll() {
        return Lists.newArrayList(expoTimeRepository.findAll());
    }

    @Override
    public ExpoTime findById(Long id) {
        return expoTimeRepository.findOne(id);
    }

    @Override
    public ExpoTime save(ExpoTime expoTime) {
        return expoTimeRepository.save(expoTime);
    }

    @Override
    public void deleteById(Long id) {
        expoTimeRepository.delete(id);
    }
}
