package ru.netlly.technologymap.service.jpa;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.netlly.technologymap.domain.Periodicity;
import ru.netlly.technologymap.repository.PeriodicityRepository;
import ru.netlly.technologymap.service.PeriodicityService;

import java.util.List;

@Service("periodicityService")
@Repository
@Transactional
public class PeriodicityServiceImpl implements PeriodicityService{

    @Autowired
    private PeriodicityRepository periodicityRepository;

    @Override
    public List<Periodicity> findAll() {
        return Lists.newArrayList(periodicityRepository.findAll());
    }

    @Override
    public Periodicity findById(Long id) {
        return periodicityRepository.findOne(id);
    }

    @Override
    public Periodicity save(Periodicity periodicity) {
        return periodicityRepository.save(periodicity);
    }

    @Override
    public void deleteById(Long id) {
        periodicityRepository.delete(id);
    }
}
