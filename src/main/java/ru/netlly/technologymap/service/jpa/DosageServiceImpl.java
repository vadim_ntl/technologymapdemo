package ru.netlly.technologymap.service.jpa;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.netlly.technologymap.domain.Dosage;
import ru.netlly.technologymap.repository.DosageRepository;
import ru.netlly.technologymap.service.DosageService;

import java.util.List;


@Service("dosageService")
@Repository
@Transactional
public class DosageServiceImpl implements DosageService{

    @Autowired
    private DosageRepository dosageRepository;

    public List<Dosage> findAll() {
        return Lists.newArrayList(dosageRepository.findAll());
    }

    @Override
    public Dosage findById(Long id) {
        return dosageRepository.findOne(id);
    }

    @Override
    public Dosage save(Dosage dosage) {
        return dosageRepository.save(dosage);
    }

    @Override
    public void deleteById(Long id) {
        dosageRepository.delete(id);
    }
}
