package ru.netlly.technologymap.service.jpa;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.netlly.technologymap.domain.Usage;
import ru.netlly.technologymap.repository.UsageRepository;
import ru.netlly.technologymap.service.UsageService;

import java.util.List;

@Service("usageService")
@Repository
@Transactional
public class UsageServiceImpl implements UsageService {

    @Autowired
    private UsageRepository usageRepository;

    @Override
    public List<Usage> findAll() {
        return Lists.newArrayList(usageRepository.findAll());
    }

    @Override
    public List<Usage> findStandardAll() {
        return usageRepository.findStandardAll();
    }

    @Override
    public List<Usage> findNonstandardAll() {
        return usageRepository.findNonstandardAll();
    }

    @Override
    public Usage findById(Long id) {
        return usageRepository.findOne(id);
    }

    @Override
    public Usage save(Usage usage) {
        return usageRepository.save(usage);
    }

    @Override
    public void deleteById(Long id) {
        usageRepository.delete(id);
    }
}
