package ru.netlly.technologymap.service.jpa;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.netlly.technologymap.domain.Area;
import ru.netlly.technologymap.repository.AreaRepository;
import ru.netlly.technologymap.service.AreaService;

import java.util.List;

@Service("areaService")
@Repository
@Transactional
public class AreaServiceImpl implements AreaService {

    @Autowired
    private AreaRepository areaRepository;

    @Override
    public List<Area> findAll() {
        return Lists.newArrayList(areaRepository.findAll());
    }

    @Override
    public Area findById(Long id) {
        return areaRepository.findOne(id);
    }

    @Override
    public Area findByIdWithRelation(Long id) {
        return areaRepository.findByIdWithRelation(id);
    }

    @Override
    public Area save(Area area) {
        return areaRepository.save(area);
    }

    @Override
    public void deleteById(Long id) {
        areaRepository.delete(id);
    }
}
