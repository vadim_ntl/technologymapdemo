package ru.netlly.technologymap.service.jpa;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.netlly.technologymap.domain.Siz;
import ru.netlly.technologymap.repository.SizRepository;
import ru.netlly.technologymap.service.SizService;

import java.util.List;

@Service("sizService")
@Repository
@Transactional
public class SizServiceImpl implements SizService{

    @Autowired
    private SizRepository sizRepository;

    @Override
    public List<Siz> findAll() {
        return Lists.newArrayList(sizRepository.findAll());
    }

    @Override
    public Siz findById(Long id) {
        return sizRepository.findOne(id);
    }

    @Override
    public Siz save(Siz siz) {
        return sizRepository.save(siz);
    }

    @Override
    public void deleteById(Long id) {
        sizRepository.delete(id);
    }
}
