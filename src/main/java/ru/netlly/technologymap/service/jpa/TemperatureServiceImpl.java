package ru.netlly.technologymap.service.jpa;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.netlly.technologymap.domain.Temperature;
import ru.netlly.technologymap.repository.TemperatureRepository;
import ru.netlly.technologymap.service.TemperatureService;

import java.util.List;

@Service("temperatureService")
@Repository
@Transactional
public class TemperatureServiceImpl implements TemperatureService {

    @Autowired
    private TemperatureRepository temperatureRepository;

    @Override
    public List<Temperature> findAll() {
        return Lists.newArrayList(temperatureRepository.findAll());
    }

    @Override
    public Temperature findById(Long id) {
        return temperatureRepository.findOne(id);
    }

    @Override
    public Temperature save(Temperature temperature) {
        return temperatureRepository.save(temperature);
    }

    @Override
    public void deleteById(Long id) {
        temperatureRepository.delete(id);
    }
}
