package ru.netlly.technologymap.service.jpa;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.netlly.technologymap.domain.TechnologyMap;
import ru.netlly.technologymap.repository.TechnologyMapRepository;
import ru.netlly.technologymap.service.TechnologyMapService;

import java.util.List;

@Service("technologyMapService")
@Repository
@Transactional
public class TechnologyMapServiceImpl implements TechnologyMapService{

    @Autowired
    private TechnologyMapRepository technologyMapRepository;

    @Override
    public List<TechnologyMap> findAll() {
        return Lists.newArrayList(technologyMapRepository.findAll());
    }

    @Override
    public TechnologyMap findById(Long id) {
        return technologyMapRepository.findOne(id);
    }

    @Override
    public TechnologyMap save(TechnologyMap technologyMap) {
        return technologyMapRepository.save(technologyMap);
    }

    @Override
    public void deleteById(Long id) {
        technologyMapRepository.delete(id);
    }
}
