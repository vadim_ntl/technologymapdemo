package ru.netlly.technologymap.service;

import ru.netlly.technologymap.domain.Siz;

import java.util.List;

public interface SizService {
    public List<Siz> findAll();

    public Siz findById(Long id);

    public Siz save(Siz siz);

    public void deleteById(Long id);
}
