package ru.netlly.technologymap.service;

import ru.netlly.technologymap.domain.Purpose;

import java.util.List;

public interface PurposeService {

    public List<Purpose> findAll();

    public Purpose findById(Long id);

    public Purpose findByIdWithRelation(Long id);

    public Purpose save(Purpose purpose);

    public void deleteById(Long id);
}
