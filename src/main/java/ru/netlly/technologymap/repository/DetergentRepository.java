package ru.netlly.technologymap.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import ru.netlly.technologymap.domain.Detergent;

public interface DetergentRepository extends CrudRepository<Detergent, Long> {
    public Detergent findByDetergentName(String detergentName);

    @Query("select d from Detergent d left join fetch d.dosages dsg where d.id = :id")
    public Detergent findByIdWithRelation(@Param("id") Long id);
}
