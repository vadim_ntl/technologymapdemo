package ru.netlly.technologymap.repository;

import org.springframework.data.repository.CrudRepository;
import ru.netlly.technologymap.domain.ExpoTime;

public interface ExpoTimeRepository extends CrudRepository<ExpoTime, Long>{
}
