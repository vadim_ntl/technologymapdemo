package ru.netlly.technologymap.repository;

import org.springframework.data.repository.CrudRepository;
import ru.netlly.technologymap.domain.Temperature;

public interface TemperatureRepository extends CrudRepository<Temperature, Long>{
}
