package ru.netlly.technologymap.repository;

import org.springframework.data.repository.CrudRepository;
import ru.netlly.technologymap.domain.Periodicity;

public interface PeriodicityRepository extends CrudRepository<Periodicity, Long>{
}
