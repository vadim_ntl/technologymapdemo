package ru.netlly.technologymap.repository;

import org.springframework.data.repository.CrudRepository;
import ru.netlly.technologymap.domain.Dosage;

public interface DosageRepository extends CrudRepository<Dosage, Long>{
}
