package ru.netlly.technologymap.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import ru.netlly.technologymap.domain.TechnologyMap;

import java.util.List;

public interface TechnologyMapRepository extends CrudRepository<TechnologyMap, Long>{
}
