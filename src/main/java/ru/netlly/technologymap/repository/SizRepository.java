package ru.netlly.technologymap.repository;

import org.springframework.data.repository.CrudRepository;
import ru.netlly.technologymap.domain.Siz;

public interface SizRepository extends CrudRepository<Siz, Long>{
}
