package ru.netlly.technologymap.repository;

import org.springframework.data.repository.CrudRepository;
import ru.netlly.technologymap.domain.AreaColor;

public interface AreaColorRepository extends CrudRepository<AreaColor, Long>{
}
