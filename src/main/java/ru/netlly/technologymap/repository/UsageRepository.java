package ru.netlly.technologymap.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ru.netlly.technologymap.domain.Usage;

import java.util.List;

public interface UsageRepository extends CrudRepository<Usage, Long>{

    @Query("select u from Usage u where u.usageStandard = 'Standard'")
    public List<Usage> findStandardAll();

    @Query("select u from Usage u where u.usageStandard = 'Nonstandard'")
    public List<Usage> findNonstandardAll();
}
