package ru.netlly.technologymap.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import ru.netlly.technologymap.domain.Area;

public interface AreaRepository extends CrudRepository<Area, Long>{

    @Query("select a from Area a left join fetch a.purposes p where a.id = :id")
    public Area findByIdWithRelation(@Param("id") Long id);
}
