package ru.netlly.technologymap.web.converter;

import org.primefaces.component.selectonemenu.SelectOneMenu;
import ru.netlly.technologymap.domain.Convertible;

import javax.faces.component.UIComponent;
import javax.faces.component.UISelectItems;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.util.ArrayList;
import java.util.List;

@FacesConverter("genericSelectOneMenuConverter")
public class GenericSelectOneMenuConverter implements Converter{
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String s) {

        Object convertible = null;

        if (component instanceof SelectOneMenu) {
            List<UIComponent> childs = ((SelectOneMenu) component).getChildren();
            UISelectItems items = null;
            if (childs != null) {
                for (UIComponent ui : childs) {
                    if (ui instanceof UISelectItems) {
                        items = (UISelectItems) ui;
                        break;
                    }
                }
            }
            if (items != null) {
                for (Object o : (ArrayList) items.getValue()) {
                    String convertibleId = ((Convertible) o).getId().toString();
                    if (s.equals(convertibleId)) {
                        convertible = o;
                        break;
                    }
                }
            }
        }

        return convertible;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object o) {
        return ((Convertible) o).getId().toString();
    }
}
