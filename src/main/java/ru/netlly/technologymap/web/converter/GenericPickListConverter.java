package ru.netlly.technologymap.web.converter;

import org.primefaces.component.picklist.PickList;
import org.primefaces.model.DualListModel;
import ru.netlly.technologymap.domain.Convertible;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.persistence.Convert;

@FacesConverter("genericPickListConverter")
public class GenericPickListConverter implements Converter{
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        Object convertible = null;

        if (uiComponent instanceof PickList) {
            Object dualList = ((PickList) uiComponent).getValue();
            DualListModel dualListModel = (DualListModel) dualList;
            for (Object o : dualListModel.getSource()) {
                String convertibleId = ((Convertible) o).getId().toString();
                if (s.equals(convertibleId)) {
                    convertible = o;
                    break;
                }
            }
            if (convertible == null)
                for (Object o : dualListModel.getTarget()) {
                    String convertibleId = ((Convertible) o).getId().toString();
                    if (s.equals(convertibleId)) {
                        convertible = o;
                        break;
                    }
                }
        }

        return convertible;
    }

    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        return ((Convertible) o).getId().toString();
    }

}
