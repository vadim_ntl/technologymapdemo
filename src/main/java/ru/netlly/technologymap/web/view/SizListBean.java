package ru.netlly.technologymap.web.view;

import org.primefaces.event.RowEditEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.netlly.technologymap.domain.Siz;
import ru.netlly.technologymap.service.SizService;

import java.util.List;

@Component
@Scope("session")
public class SizListBean {
    private final static Logger logger = LoggerFactory.getLogger(SizListBean.class);

    private List<Siz> sizs;
    private Siz selectedSiz;
    private String newSizName;

    @Autowired
    private SizService sizService;

    public List<Siz> getSizs() {
        return sizs;
    }

    public void setSizs(List<Siz> sizs) {
        this.sizs = sizs;
    }

    public Siz getSelectedSiz() {
        return selectedSiz;
    }

    public void setSelectedSiz(Siz selectedSiz) {
        this.selectedSiz = selectedSiz;
    }

    public String getNewSizName() {
        return newSizName;
    }

    public void setNewSizName(String newSizName) {
        this.newSizName = newSizName;
    }

    public void init() {
        sizs = sizService.findAll();
    }

    public void addSiz() {
        Siz siz = new Siz(newSizName);
        siz = sizService.save(siz);
        sizs.add(siz);
        newSizName = null;
    }

    public void onRowEdit(RowEditEvent event) {
        Siz siz = (Siz) event.getObject();
        sizService.save(siz);
    }

    public void removeSiz(Siz siz) {
        sizService.deleteById(siz.getId());
        sizs.remove(siz);
    }
}
