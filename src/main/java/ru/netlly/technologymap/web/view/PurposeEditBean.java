package ru.netlly.technologymap.web.view;

import org.primefaces.model.DualListModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.webflow.execution.RequestContext;
import ru.netlly.technologymap.domain.Detergent;
import ru.netlly.technologymap.domain.Purpose;
import ru.netlly.technologymap.service.DetergentService;
import ru.netlly.technologymap.service.PurposeService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Component
@Scope("session")
public class PurposeEditBean {

    private Purpose purpose;
    private DualListModel<Detergent> detergents;

    @Autowired
    private PurposeService purposeService;

    @Autowired
    private DetergentService detergentService;

    public Purpose getPurpose() {
        return purpose;
    }

    public void setPurpose(Purpose purpose) {
        this.purpose = purpose;
    }

    public DualListModel<Detergent> getDetergents() {
        return detergents;
    }

    public void setDetergents(DualListModel<Detergent> detergents) {
        this.detergents = detergents;
    }

    public void init(RequestContext context) {

        Long id = context.getRequestScope().getLong("purposeEditId");
        purpose = purposeService.findByIdWithRelation(id);

        List<Detergent> detergentsSource = new ArrayList<Detergent>();
        List<Detergent> detergentsTarget = new ArrayList<Detergent>();
        for (Detergent detergent : purpose.getDetergents()) {
            detergentsTarget.add(detergent);
        }
        for (Detergent detergent : detergentService.findAll()) {
            if (!(detergentsTarget.contains(detergent))) {
                detergentsSource.add(detergent);
            }
        }
        detergents = new DualListModel<Detergent>(detergentsSource, detergentsTarget);
    }

    public void save() {
        purpose.setDetergents(new HashSet<Detergent>(detergents.getTarget()));
        purposeService.save(purpose);
    }
}
