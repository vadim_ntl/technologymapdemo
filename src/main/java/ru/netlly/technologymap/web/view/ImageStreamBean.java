package ru.netlly.technologymap.web.view;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.netlly.technologymap.domain.Detergent;
import ru.netlly.technologymap.domain.Dosage;
import ru.netlly.technologymap.service.DetergentService;
import ru.netlly.technologymap.service.DosageService;

import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import java.io.ByteArrayInputStream;
import java.io.Serializable;

@Component
@Scope("session")
public class ImageStreamBean implements Serializable{

    @Autowired
    private DosageService dosageService;

    @Autowired
    private DetergentService detergentService;

    public StreamedContent getDosageImage() {
        FacesContext facesContext = FacesContext.getCurrentInstance();

        if (facesContext.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            return new DefaultStreamedContent();
        } else {
            Long imageId = Long.valueOf(facesContext.getExternalContext().getRequestParameterMap().get("dosageId"));
            Dosage dosage = dosageService.findById(imageId);
            return new DefaultStreamedContent(new ByteArrayInputStream(dosage.getImage()));
        }
    }

    public StreamedContent getDetergentImage() {
        FacesContext facesContext = FacesContext.getCurrentInstance();

        if (facesContext.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            return new DefaultStreamedContent();
        } else {
            Long imageId = Long.valueOf(facesContext.getExternalContext().getRequestParameterMap().get("detergentId"));
            Detergent detergent = detergentService.findById(imageId);
            return new DefaultStreamedContent(new ByteArrayInputStream(detergent.getImage()));
        }
    }
}
