package ru.netlly.technologymap.web.view;

import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.netlly.technologymap.domain.Usage;
import ru.netlly.technologymap.service.UsageService;

import java.util.List;

@Component
@Scope("session")
public class UsageListBean {
    private List<Usage> usages;
    private Usage selectedUsage;
    private String addUsageName;
    private String addUsageStandard;
    private String[] standards = Usage.standards;

    @Autowired
    private UsageService usageService;

    public List<Usage> getUsages() {
        return usages;
    }

    public void setUsages(List<Usage> usages) {
        this.usages = usages;
    }

    public Usage getSelectedUsage() {
        return selectedUsage;
    }

    public void setSelectedUsage(Usage selectedUsage) {
        this.selectedUsage = selectedUsage;
    }

    public String getAddUsageName() {
        return addUsageName;
    }

    public void setAddUsageName(String addUsageName) {
        this.addUsageName = addUsageName;
    }

    public String getAddUsageStandard() {
        return addUsageStandard;
    }

    public void setAddUsageStandard(String addUsageStandard) {
        this.addUsageStandard = addUsageStandard;
    }

    public String[] getStandards() {
        return standards;
    }

    public void init() {
        usages = usageService.findAll();
    }

    public void addUsage() {
        Usage usage = new Usage(addUsageName, addUsageStandard);
        usage = usageService.save(usage);
        usages.add(usage);

        addUsageName = null;
        addUsageStandard = null;
    }

    public void onRowEdit(RowEditEvent event) {
        Usage usage = (Usage) event.getObject();
        usageService.save(usage);
    }

    public void removeUsage(Usage usage) {
        usageService.deleteById(usage.getId());
        usages.remove(usage);
    }
}
