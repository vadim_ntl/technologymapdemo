package ru.netlly.technologymap.web.view;

import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.netlly.technologymap.domain.Area;
import ru.netlly.technologymap.service.AreaService;

import java.io.Serializable;
import java.util.List;

@Component
@Scope("session")
public class AreaListBean implements Serializable{
    private List<Area> areas;
    private Area selectedArea;

    @Autowired
    private AreaService areaService;

    public List<Area> getAreas() {
        return areas;
    }

    public void setAreas(List<Area> areas) {
        this.areas = areas;
    }

    public Area getSelectedArea() {
        return selectedArea;
    }

    public void setSelectedArea(Area selectedArea) {
        this.selectedArea = selectedArea;
    }

    public void init() {
        areas = areaService.findAll();
    }

    public void removeArea(Area area) {
        areaService.deleteById(area.getId());
        areas.remove(area);
    }

    public void onRowEdit(RowEditEvent event) {
        Area area = (Area) event.getObject();
        areaService.save(area);
    }
}
