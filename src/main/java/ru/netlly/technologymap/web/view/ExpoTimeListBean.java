package ru.netlly.technologymap.web.view;

import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.netlly.technologymap.domain.ExpoTime;
import ru.netlly.technologymap.service.ExpoTimeService;

import java.util.List;

@Component
@Scope("session")
public class ExpoTimeListBean {
    private List<ExpoTime> expoTimes;
    private ExpoTime selectedExpoTime;
    private String newExpoTimeValue;

    @Autowired
    private ExpoTimeService expoTimeService;

    public List<ExpoTime> getExpoTimes() {
        return expoTimes;
    }

    public void setExpoTimes(List<ExpoTime> expoTimes) {
        this.expoTimes = expoTimes;
    }

    public ExpoTime getSelectedExpoTime() {
        return selectedExpoTime;
    }

    public void setSelectedExpoTime(ExpoTime selectedExpoTime) {
        this.selectedExpoTime = selectedExpoTime;
    }

    public String getNewExpoTimeValue() {
        return newExpoTimeValue;
    }

    public void setNewExpoTimeValue(String newExpoTimeValue) {
        this.newExpoTimeValue = newExpoTimeValue;
    }

    public void init() {
        expoTimes = expoTimeService.findAll();
    }

    public void addExpoTime() {
        ExpoTime expoTime = new ExpoTime(newExpoTimeValue);
        expoTime = expoTimeService.save(expoTime);
        expoTimes.add(expoTime);
        newExpoTimeValue = null;
    }

    public void onRowEdit(RowEditEvent event) {
        ExpoTime expoTime = (ExpoTime) event.getObject();
        expoTimeService.save(expoTime);
    }

    public void removeExpoTime(ExpoTime expoTime) {
        expoTimeService.deleteById(expoTime.getId());
        expoTimes.remove(expoTime);
    }
}
