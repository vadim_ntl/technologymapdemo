package ru.netlly.technologymap.web.view;

import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.netlly.technologymap.domain.Purpose;
import ru.netlly.technologymap.service.PurposeService;

import java.io.Serializable;
import java.util.List;

@Component
@Scope("session")
public class PurposeListBean implements Serializable{
    private List<Purpose> purposes;
    private Purpose selectedPurpose;

    @Autowired
    private PurposeService purposeService;

    public List<Purpose> getPurposes() {
        return purposes;
    }

    public void setPurposes(List<Purpose> purposes) {
        this.purposes = purposes;
    }

    public Purpose getSelectedPurpose() {
        return selectedPurpose;
    }

    public void setSelectedPurpose(Purpose selectedPurpose) {
        this.selectedPurpose = selectedPurpose;
    }

    public void init() {
        purposes = purposeService.findAll();
    }

    public void removePurpose(Purpose purpose) {
        purposeService.deleteById(purpose.getId());
        purposes.remove(purpose);
    }

    public void onRowEdit(RowEditEvent event) {
        Purpose purpose = (Purpose) event.getObject();
        purposeService.save(purpose);
    }
}
