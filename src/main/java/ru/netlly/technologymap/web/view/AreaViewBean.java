package ru.netlly.technologymap.web.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.webflow.execution.RequestContext;
import ru.netlly.technologymap.domain.Area;
import ru.netlly.technologymap.service.AreaService;

import java.io.Serializable;

@Component
@Scope("session")
public class AreaViewBean implements Serializable{
    private Area area;

    @Autowired
    private AreaService areaService;

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public void init(RequestContext requestContext) {
        Long id = requestContext.getRequestScope().getLong("areaViewId");
        area = areaService.findByIdWithRelation(id);
    }
}

