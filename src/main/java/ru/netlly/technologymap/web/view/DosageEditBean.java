package ru.netlly.technologymap.web.view;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.webflow.execution.RequestContext;
import ru.netlly.technologymap.domain.Dosage;
import ru.netlly.technologymap.service.DosageService;

import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import java.io.ByteArrayInputStream;

@Component
@Scope("session")
public class DosageEditBean {
    private Dosage dosage;

    @Autowired
    private DosageService dosageService;

    public Dosage getDosage() {
        return dosage;
    }

    public void setDosage(Dosage dosage) {
        this.dosage = dosage;
    }

    public StreamedContent getImage() {

        FacesContext facesContext = FacesContext.getCurrentInstance();

        if (facesContext.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            return new DefaultStreamedContent();
        } else {
            return new DefaultStreamedContent(new ByteArrayInputStream(dosage.getImage()));
        }

    }

    public void handleFileUpload(FileUploadEvent event) {
        dosage.setImage(event.getFile().getContents());
    }

    public void init(RequestContext requestContext) {
        Long dosageId = requestContext.getRequestScope().getLong("dosageEditId");
        dosage = dosageService.findById(dosageId);
    }

    public void save() {
        dosageService.save(dosage);
    }
}
