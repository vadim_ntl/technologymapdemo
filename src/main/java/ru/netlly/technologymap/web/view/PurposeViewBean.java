package ru.netlly.technologymap.web.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.webflow.execution.RequestContext;
import ru.netlly.technologymap.domain.Purpose;
import ru.netlly.technologymap.service.PurposeService;

@Component
@Scope("session")
public class PurposeViewBean {
    private Purpose purpose;

    @Autowired
    private PurposeService purposeService;

    public Purpose getPurpose() {
        return purpose;
    }

    public void setPurpose(Purpose purpose) {
        this.purpose = purpose;
    }

    public void init(RequestContext context) {
        Long id = context.getRequestScope().getLong("purposeViewId");
        purpose = purposeService.findByIdWithRelation(id);
    }
}
