package ru.netlly.technologymap.web.view;

import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.netlly.technologymap.domain.Periodicity;
import ru.netlly.technologymap.service.PeriodicityService;

import java.util.List;

@Component
@Scope("session")
public class PeriodicityListBean {
    private List<Periodicity> periodicities;
    private Periodicity selectedPeriodicity;
    private String newPeriodicityName;

    @Autowired
    private PeriodicityService periodicityService;

    public List<Periodicity> getPeriodicities() {
        return periodicities;
    }

    public void setPeriodicities(List<Periodicity> periodicities) {
        this.periodicities = periodicities;
    }

    public Periodicity getSelectedPeriodicity() {
        return selectedPeriodicity;
    }

    public void setSelectedPeriodicity(Periodicity selectedPeriodicity) {
        this.selectedPeriodicity = selectedPeriodicity;
    }

    public String getNewPeriodicityName() {
        return newPeriodicityName;
    }

    public void setNewPeriodicityName(String newPeriodicityName) {
        this.newPeriodicityName = newPeriodicityName;
    }

    public void init() {
        periodicities = periodicityService.findAll();
    }

    public void addPeriodicity() {
        Periodicity periodicity = new Periodicity(newPeriodicityName);
        periodicity = periodicityService.save(periodicity);
        periodicities.add(periodicity);
        newPeriodicityName = null;
    }

    public void onRowEdit(RowEditEvent event) {
        Periodicity periodicity = (Periodicity) event.getObject();
        periodicityService.save(periodicity);
    }

    public void removePeriodicity(Periodicity periodicity) {
        periodicityService.deleteById(periodicity.getId());
        periodicities.remove(periodicity);
    }
}
