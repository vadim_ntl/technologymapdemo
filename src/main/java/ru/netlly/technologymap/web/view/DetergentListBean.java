package ru.netlly.technologymap.web.view;

import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.netlly.technologymap.domain.Detergent;
import ru.netlly.technologymap.service.DetergentService;

import java.io.Serializable;
import java.util.List;

@Component
@Scope("session")
public class DetergentListBean implements Serializable {
    private List<Detergent> detergents;
    private Detergent selectedDetergent;

    @Autowired
    private DetergentService detergentService;

    public List<Detergent> getDetergents() {
        return detergents;
    }

    public void setDetergents(List<Detergent> detergents) {
        this.detergents = detergents;
    }

    public Detergent getSelectedDetergent() {
        return selectedDetergent;
    }

    public void setSelectedDetergent(Detergent selectedDetergent) {
        this.selectedDetergent = selectedDetergent;
    }

    public void init() {
        detergents = detergentService.findAll();
    }

    public void removeDetergent(Detergent detergent) {
        detergentService.deleteById(detergent.getId());
        detergents.remove(detergent);
    }

    public void onRowEdit(RowEditEvent event) {
        Detergent detergent = (Detergent) event.getObject();
        detergentService.save(detergent);
    }
}
