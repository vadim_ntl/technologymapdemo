package ru.netlly.technologymap.web.view;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.RowEditEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.netlly.technologymap.domain.Dosage;
import ru.netlly.technologymap.service.DosageService;

import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.List;

@Component
@Scope("session")
public class DosageListBean implements Serializable{
    private static final Logger logger = LoggerFactory.getLogger(DosageListBean.class);

    private List<Dosage> dosages;
    private Dosage selectedDosage;
    private String addDosageContent;
    private byte[] addDosageImage;

    @Autowired
    private DosageService dosageService;

    public void init(){
        dosages = dosageService.findAll();
    }

    public List<Dosage> getDosages() {
        return dosages;
    }

    public void setDosages(List<Dosage> dosages) {
        this.dosages = dosages;
    }

    public Dosage getSelectedDosage() {
        return selectedDosage;
    }

    public void setSelectedDosage(Dosage selectedDosage) {
        this.selectedDosage = selectedDosage;
    }

    public String getAddDosageContent() {
        return addDosageContent;
    }

    public void setAddDosageContent(String addDosageContent) {
        this.addDosageContent = addDosageContent;
    }

    public void addDosage() {
        Dosage dosage = new Dosage(addDosageContent);
        dosage.setImage(addDosageImage);
        dosage = dosageService.save(dosage);
        dosages.add(dosage);
        addDosageContent = null;
        addDosageImage = null;
    }

    public void onRowEdit(RowEditEvent event) {
        Dosage dosage = (Dosage) event.getObject();
        dosageService.save(dosage);
    }

    public void removeDosage(Dosage dosage) {
        dosageService.deleteById(dosage.getId());
        dosages.remove(dosage);
    }

    public StreamedContent getAddDosageImage() {
        FacesContext facesContext = FacesContext.getCurrentInstance();

        if (facesContext.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            return new DefaultStreamedContent();
        } else {
            return new DefaultStreamedContent(new ByteArrayInputStream(addDosageImage));
        }
    }

    public void handleAddDosageFileUpload(FileUploadEvent event) {
        addDosageImage = event.getFile().getContents();
    }

    //Inline image get

    public StreamedContent getImage() {
        FacesContext facesContext = FacesContext.getCurrentInstance();

        if (facesContext.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            return new DefaultStreamedContent();
        } else {
            Long imageId = Long.valueOf(facesContext.getExternalContext().getRequestParameterMap().get("imageId"));
            Dosage dosage = dosageService.findById(imageId);
            return new DefaultStreamedContent(new ByteArrayInputStream(dosage.getImage()));
        }
    }


}
