package ru.netlly.technologymap.web.view;

import org.primefaces.model.DualListModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.netlly.technologymap.domain.*;
import ru.netlly.technologymap.service.*;

import java.io.Serializable;
import java.util.*;

@Component
@Scope("session")
public class TechnologyMapAddBean implements Serializable{
    private static final Logger logger = LoggerFactory.getLogger(TechnologyMapAddBean.class);

    private TechnologyMap technologyMap;
    private List<AreaColor> areaColors;
    private List<Area> areas;
    private List<Purpose> purposes;
    private List<Detergent> detergents;
    private List<Dosage> dosages;

    private String[] standards = Usage.standards;
    private String standard;
    private List<Usage> standardUsages;
    private Usage selectedStandardUsage;
    private DualListModel<Usage> nonStandardUsages;

    private List<Periodicity> periodicities;
    private List<Temperature> temperatures;
    private List<ExpoTime> expoTimes;
    private List<Siz> sizs;

    @Autowired
    private TechnologyMapService technologyMapService;

    @Autowired
    private AreaColorService areaColorService;

    @Autowired
    private AreaService areaService;

    @Autowired
    private PurposeService purposeService;

    @Autowired
    private DetergentService detergentService;

    @Autowired
    private DosageService dosageService;

    @Autowired
    private UsageService usageService;

    @Autowired
    private PeriodicityService periodicityService;

    @Autowired
    private TemperatureService temperatureService;

    @Autowired
    private ExpoTimeService expoTimeService;

    @Autowired
    private SizService sizService;

    //start getters and setters

    public TechnologyMap getTechnologyMap() {
        return technologyMap;
    }

    public void setTechnologyMap(TechnologyMap technologyMap) {
        this.technologyMap = technologyMap;
    }

    public List<AreaColor> getAreaColors() {
        return areaColors;
    }

    public void setAreaColors(List<AreaColor> areaColors) {
        this.areaColors = areaColors;
    }

    public List<Area> getAreas() {
        return areas;
    }

    public void setAreas(List<Area> areas) {
        this.areas = areas;
    }

    public List<Purpose> getPurposes() {
        return purposes;
    }

    public void setPurposes(List<Purpose> purposes) {
        this.purposes = purposes;
    }

    public List<Detergent> getDetergents() {
        return detergents;
    }

    public void setDetergents(List<Detergent> detergents) {
        this.detergents = detergents;
    }

    public List<Dosage> getDosages() {
        return dosages;
    }

    public void setDosages(List<Dosage> dosages) {
        this.dosages = dosages;
    }

    public List<Usage> getStandardUsages() {
        return standardUsages;
    }

    public void setStandardUsages(List<Usage> standardUsages) {
        this.standardUsages = standardUsages;
    }

    public DualListModel<Usage> getNonStandardUsages() {
        return nonStandardUsages;
    }

    public void setNonStandardUsages(DualListModel<Usage> nonStandardUsages) {
        this.nonStandardUsages = nonStandardUsages;
    }

    public Usage getSelectedStandardUsage() {
        return selectedStandardUsage;
    }

    public void setSelectedStandardUsage(Usage selectedStandardUsage) {
        this.selectedStandardUsage = selectedStandardUsage;
    }

    public String[] getStandards() {
        return standards;
    }

    public void setStandards(String[] standards) {
        this.standards = standards;
    }

    public String getStandard() {
        return standard;
    }

    public void setStandard(String standard) {
        this.standard = standard;
    }

    public List<Periodicity> getPeriodicities() {
        return periodicities;
    }

    public void setPeriodicities(List<Periodicity> periodicities) {
        this.periodicities = periodicities;
    }

    public List<Temperature> getTemperatures() {
        return temperatures;
    }

    public void setTemperatures(List<Temperature> temperatures) {
        this.temperatures = temperatures;
    }

    public List<ExpoTime> getExpoTimes() {
        return expoTimes;
    }

    public void setExpoTimes(List<ExpoTime> expoTimes) {
        this.expoTimes = expoTimes;
    }

    public List<Siz> getSizs() {
        return sizs;
    }

    public void setSizs(List<Siz> sizs) {
        this.sizs = sizs;
    }

    //end getters and setters

    public void init() {
        technologyMap = new TechnologyMap();

        areaColors = areaColorService.findAll();
        areas = areaService.findAll();
        purposes = purposeService.findAll();
        periodicities = periodicityService.findAll();
        temperatures = temperatureService.findAll();
        expoTimes = expoTimeService.findAll();
        sizs = sizService.findAll();

        standardUsages = usageService.findStandardAll();

        List<Usage> usagesSource = new ArrayList<Usage>();
        List<Usage> usagesTarget = new ArrayList<Usage>();
        for (Usage usage : usageService.findNonstandardAll()) {
            usagesSource.add(usage);
        }
        nonStandardUsages = new DualListModel<Usage>(usagesSource, usagesTarget);
    }

    public void add() {

        HashSet<TechMapUsage> techMapUsages = new HashSet<TechMapUsage>();
        if (standard.equals("Standard")) {
            TechMapUsageId techMapUsageId = new TechMapUsageId(technologyMap, selectedStandardUsage);
            TechMapUsage techMapUsage = new TechMapUsage(techMapUsageId, 1);
            techMapUsages.add(techMapUsage);
        } else if (standard.equals("Nonstandard")) {
            Integer i = 1;
            for (Usage usage : nonStandardUsages.getTarget()) {
                TechMapUsageId techMapUsageId = new TechMapUsageId(technologyMap, usage);
                TechMapUsage techMapUsage = new TechMapUsage(techMapUsageId, i++);
                techMapUsages.add(techMapUsage);
            }
        }
        technologyMap.setTechMapUsages(techMapUsages);

        technologyMapService.save(technologyMap);
    }

    public void onAreaChange() {
        Area area = areaService.findByIdWithRelation(technologyMap.getArea().getId());
        purposes = new ArrayList<Purpose>(area.getPurposes());
    }

    public void onPurposeChange() {
        Purpose purpose = purposeService.findByIdWithRelation(technologyMap.getPurpose().getId());
        detergents = new ArrayList<Detergent>(purpose.getDetergents());
    }

    public void onDetergentChange() {
        Detergent detergent = detergentService.findByIdWithRelation(technologyMap.getDetergent().getId());
        dosages = new ArrayList<Dosage>(detergent.getDosages());
    }

}
