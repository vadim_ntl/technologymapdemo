package ru.netlly.technologymap.web.view;

import org.primefaces.model.DualListModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.netlly.technologymap.domain.Area;
import ru.netlly.technologymap.domain.Purpose;
import ru.netlly.technologymap.service.PurposeService;
import ru.netlly.technologymap.service.AreaService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Component
@Scope("session")
public class AreaAddBean implements Serializable{
    private Area area;
    private DualListModel<Purpose> purposes;

    @Autowired
    private AreaService areaService;

    @Autowired
    private PurposeService purposeService;

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public DualListModel<Purpose> getPurposes() {
        return purposes;
    }

    public void setPurposes(DualListModel<Purpose> purposes) {
        this.purposes = purposes;
    }

    public void init() {
        area = new Area();

        List<Purpose> purposesSource = new ArrayList<Purpose>();
        List<Purpose> purposesTarget = new ArrayList<Purpose>();
        for (Purpose p : purposeService.findAll()) {
            purposesSource.add(p);
        }
        purposes = new DualListModel<Purpose>(purposesSource, purposesTarget);
    }

    public void save() {
        area.setPurposes(new HashSet<Purpose>(purposes.getTarget()));
        areaService.save(area);
    }
}
