package ru.netlly.technologymap.web.view;

import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.netlly.technologymap.domain.Temperature;
import ru.netlly.technologymap.service.TemperatureService;

import java.util.List;

@Component
@Scope("session")
public class TemperatureListBean {
    private List<Temperature> temperatures;
    private Temperature selectedTemperature;
    private String newTemperatureValue;

    @Autowired
    private TemperatureService temperatureService;

    public List<Temperature> getTemperatures() {
        return temperatures;
    }

    public void setTemperatures(List<Temperature> temperatures) {
        this.temperatures = temperatures;
    }

    public Temperature getSelectedTemperature() {
        return selectedTemperature;
    }

    public void setSelectedTemperature(Temperature selectedTemperature) {
        this.selectedTemperature = selectedTemperature;
    }

    public String getNewTemperatureValue() {
        return newTemperatureValue;
    }

    public void setNewTemperatureValue(String newTemperatureValue) {
        this.newTemperatureValue = newTemperatureValue;
    }

    public void init() {
        temperatures = temperatureService.findAll();
    }

    public void addTemperature() {
        Temperature temperature = new Temperature(newTemperatureValue);
        temperature = temperatureService.save(temperature);
        temperatures.add(temperature);
        newTemperatureValue = null;
    }

    public void onRowEdit(RowEditEvent event) {
        Temperature temperature = (Temperature) event.getObject();
        temperatureService.save(temperature);
    }

    public void removeTemperature(Temperature temperature) {
        temperatureService.deleteById(temperature.getId());
        temperatures.remove(temperature);
    }
}
