package ru.netlly.technologymap.web.view;

import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.netlly.technologymap.domain.AreaColor;
import ru.netlly.technologymap.service.AreaColorService;

import java.util.List;

@Component
@Scope("session")
public class AreaColorListBean {
    private List<AreaColor> areaColors;
    private AreaColor selectedAreaColor;
    private String addAreaColorName;
    private String addAreaColorValue;

    @Autowired
    private AreaColorService areaColorService;

    public List<AreaColor> getAreaColors() {
        return areaColors;
    }

    public void setAreaColors(List<AreaColor> areaColors) {
        this.areaColors = areaColors;
    }

    public AreaColor getSelectedAreaColor() {
        return selectedAreaColor;
    }

    public void setSelectedAreaColor(AreaColor selectedAreaColor) {
        this.selectedAreaColor = selectedAreaColor;
    }

    public String getAddAreaColorName() {
        return addAreaColorName;
    }

    public void setAddAreaColorName(String addAreaColorName) {
        this.addAreaColorName = addAreaColorName;
    }

    public String getAddAreaColorValue() {
        return addAreaColorValue;
    }

    public void setAddAreaColorValue(String addAreaColorValue) {
        this.addAreaColorValue = addAreaColorValue;
    }

    public void init() {
        areaColors = areaColorService.findAll();
    }

    public void addAreaColor() {
        AreaColor areaColor = new AreaColor(addAreaColorName, addAreaColorValue);
        areaColor = areaColorService.save(areaColor);
        areaColors.add(areaColor);

        addAreaColorName = null;
        addAreaColorValue = null;
    }

    public void onRowEdit(RowEditEvent event) {
        AreaColor areaColor = (AreaColor) event.getObject();
        areaColorService.save(areaColor);
    }

    public void removeAreaColor(AreaColor areaColor) {
        areaColorService.deleteById(areaColor.getId());
        areaColors.remove(areaColor);
    }
}
