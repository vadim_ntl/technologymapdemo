package ru.netlly.technologymap.web.view;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.webflow.execution.RequestContext;
import ru.netlly.technologymap.domain.Detergent;
import ru.netlly.technologymap.service.DetergentService;

import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import java.io.ByteArrayInputStream;
import java.io.Serializable;

@Component
@Scope("session")
public class DetergentViewBean implements Serializable{
    private static final Logger logger = LoggerFactory.getLogger(DetergentViewBean.class);

    private Detergent detergent;

    @Autowired
    private DetergentService detergentService;

    public Detergent getDetergent() {
        return detergent;
    }

    public void setDetergent(Detergent detergent) {
        this.detergent = detergent;
    }

    public StreamedContent getImage() {

        FacesContext context = FacesContext.getCurrentInstance();

        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {

            return new DefaultStreamedContent();

        } else {

            return new DefaultStreamedContent(new ByteArrayInputStream(detergent.getImage()));
        }
    }

    public void init(RequestContext requestContext) {
        Long id = requestContext.getRequestScope().getLong("detergentViewId");
        detergent = detergentService.findByIdWithRelation(id);
    }
}
