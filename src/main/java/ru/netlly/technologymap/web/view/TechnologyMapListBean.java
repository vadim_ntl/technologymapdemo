package ru.netlly.technologymap.web.view;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.netlly.technologymap.domain.TechnologyMap;
import ru.netlly.technologymap.service.TechnologyMapService;

import java.io.Serializable;
import java.util.List;

@Component
@Scope("session")
public class TechnologyMapListBean implements Serializable{

    private final static Logger logger = LoggerFactory.getLogger(TechnologyMapListBean.class);

    private List<TechnologyMap> technologyMaps;
    private TechnologyMap selectedTechnologyMap;

    @Autowired
    private TechnologyMapService technologyMapService;

    public List<TechnologyMap> getTechnologyMaps() {
        return technologyMaps;
    }

    public void setTechnologyMaps(List<TechnologyMap> technologyMaps) {
        this.technologyMaps = technologyMaps;
    }

    public TechnologyMap getSelectedTechnologyMap() {
        return selectedTechnologyMap;
    }

    public void setSelectedTechnologyMap(TechnologyMap selectedTechnologyMap) {
        this.selectedTechnologyMap = selectedTechnologyMap;
    }

    public void init() {
        technologyMaps = technologyMapService.findAll();
    }

    public void removeTechnologyMap(TechnologyMap technologyMap) {
        technologyMapService.deleteById(technologyMap.getId());
        technologyMaps.remove(technologyMap);
    }
}
