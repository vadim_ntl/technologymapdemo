package ru.netlly.technologymap.web.view;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.DualListModel;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.netlly.technologymap.domain.Detergent;
import ru.netlly.technologymap.domain.Dosage;
import ru.netlly.technologymap.service.DetergentService;
import ru.netlly.technologymap.service.DosageService;

import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Component
@Scope("session")
public class DetergentAddBean implements Serializable{
    private static final Logger logger = LoggerFactory.getLogger(DetergentAddBean.class);

    private Detergent detergent;
    private DualListModel<Dosage> dosages;

    @Autowired
    private DetergentService detergentService;

    @Autowired
    private DosageService dosageService;

    public Detergent getDetergent() {
        return detergent;
    }

    public void setDetergent(Detergent detergent) {
        this.detergent = detergent;
    }

    public DualListModel<Dosage> getDosages() {
        return dosages;
    }

    public void setDosages(DualListModel<Dosage> dosages) {
        this.dosages = dosages;
    }

    public StreamedContent getImage() {

        FacesContext facesContext = FacesContext.getCurrentInstance();

        if (facesContext.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            return new DefaultStreamedContent();
        } else {
            return new DefaultStreamedContent(new ByteArrayInputStream(detergent.getImage()));
        }
    }

    public void handleFileUpload(FileUploadEvent event) {
        detergent.setImage(event.getFile().getContents());

        logger.info("handleFileUpload: Save photo with size: {}", detergent.getImage().length);
    }

    public void init() {
        detergent = new Detergent();

        List<Dosage> dosagesSource = new ArrayList<Dosage>();
        List<Dosage> dosagesTarget = new ArrayList<Dosage>();
        for (Dosage dosage : dosageService.findAll()) {
            dosagesSource.add(dosage);
        }
        dosages = new DualListModel<Dosage>(dosagesSource, dosagesTarget);
    }

    public void save() {
        detergent.setDosages(new HashSet<Dosage>(dosages.getTarget()));
        detergentService.save(detergent);
    }
}

