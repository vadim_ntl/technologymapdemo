package ru.netlly.technologymap.domain;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "AREA")
public class Area implements Serializable, Convertible {
    private Long id;
    private String areaName;
    private Set<Purpose> purposes;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "AREA_NAME")
    @Size(min = 3, max = 80, message = "{validation.area.AreaName.Size.message}")
    @NotEmpty(message = "{validation.area.AreaName.NotEmpty.message}")
    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    @ManyToMany
    @JoinTable(
            name = "AREA_PURPOSE_RELATION",
            joinColumns = @JoinColumn(name = "AREA_ID"),
            inverseJoinColumns = @JoinColumn(name = "PURPOSE_ID")
    )
    public Set<Purpose> getPurposes() {
        return purposes;
    }

    public void setPurposes(Set<Purpose> purposes) {
        this.purposes = purposes;
    }

    @Transient
    public List<Purpose> getPurposesAsList() {
        return new ArrayList<Purpose>(purposes);
    }

    @Override
    public String toString() {
        return "Area{" +
                "id=" + id +
                ", areaName='" + areaName + '\'' +
                '}';
    }
}
