package ru.netlly.technologymap.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "TECHNOLOGYMAP_USAGE_RELATION")
@AssociationOverrides({
        @AssociationOverride(name = "pk.technologyMap", joinColumns = @JoinColumn(name = "TECHNOLOGYMAP_ID")),
        @AssociationOverride(name = "pk.usage", joinColumns = @JoinColumn(name = "USAGE_ID")) })
public class TechMapUsage implements Serializable{
    private TechMapUsageId pk = new TechMapUsageId();
    private Integer orderNo;

    public TechMapUsage() {
    }

    public TechMapUsage(TechMapUsageId pk, Integer orderNo) {
        this.pk = pk;
        this.orderNo = orderNo;
    }

    @EmbeddedId
    public TechMapUsageId getPk() {
        return pk;
    }

    public void setPk(TechMapUsageId pk) {
        this.pk = pk;
    }

    @Transient
    public TechnologyMap getTechnologyMap() {
        return getPk().getTechnologyMap();
    }

    public void setTechnologyMap(TechnologyMap technologyMap) {
        getPk().setTechnologyMap(technologyMap);
    }

    @Transient
    public Usage getUsage() {
        return getPk().getUsage();
    }

    public void setUsage(Usage usage) {
        getPk().setUsage(usage);
    }

    @Column(name = "ORDERNO")
    public Integer getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(Integer orderNo) {
        this.orderNo = orderNo;
    }

    @Override
    public String toString() {
        return "TechMapUsage{" +
                ", Usage=" + pk.getUsage() +
                ", orderNo=" + orderNo +
                '}';
    }
}
