package ru.netlly.technologymap.domain;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "SIZ")
public class Siz implements Serializable, Convertible{
    private Long id;
    private String sizName;

    public Siz() {
    }

    public Siz(String sizName) {
        this.sizName = sizName;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "SIZ_NAME")
    @Size(min = 1, max = 10, message = "{validation.siz.SizName.Size.message}")
    @NotEmpty(message = "{validation.siz.SizName.NotEmpty.message}")
    public String getSizName() {
        return sizName;
    }

    public void setSizName(String sizName) {
        this.sizName = sizName;
    }

    @Override
    public String toString() {
        return "Siz{" +
                "id=" + id +
                ", sizName='" + sizName + '\'' +
                '}';
    }
}
