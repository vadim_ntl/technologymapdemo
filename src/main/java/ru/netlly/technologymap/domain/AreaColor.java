package ru.netlly.technologymap.domain;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name = "AREACOLOR")
public class AreaColor implements Serializable, Convertible{
    private Long id;
    private String areaColorName;
    private String areaColorValue;

    public AreaColor() {
    }

    public AreaColor(String areaColorName, String areaColorValue) {
        this.areaColorName = areaColorName;
        this.areaColorValue = areaColorValue;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "AREACOLOR_NAME")
    public String getAreaColorName() {
        return areaColorName;
    }

    public void setAreaColorName(String areaColorName) {
        this.areaColorName = areaColorName;
    }

    @Column(name = "AREACOLOR_VALUE")
    public String getAreaColorValue() {
        return areaColorValue;
    }

    public void setAreaColorValue(String areaColorValue) {
        this.areaColorValue = areaColorValue;
    }

    @Override
    public String toString() {
        return "AreaColor{" +
                "id=" + id +
                ", areaColorName='" + areaColorName + '\'' +
                ", areaColorValue='" + areaColorValue + '\'' +
                '}';
    }
}
