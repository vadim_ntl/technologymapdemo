package ru.netlly.technologymap.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "DETERGENT")
public class Detergent implements Serializable, Convertible{
    private Long id;
    private String detergentShortName;
    private String detergentName;
    private byte[] image;
    private Set<Purpose> purposes = new HashSet<Purpose>();
    private Set<Dosage> dosages = new HashSet<Dosage>();

    public Detergent() {
    }

    public Detergent(String detergentShortName) {
        this.detergentShortName = detergentShortName;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "DETERGENT_SHORT_NAME")
    @Size(min=3, max = 80, message = "{validation.detergent.DetergentShortName.Size.message}")
    @NotEmpty(message = "{validation.detergent.DetergentShortName.NotEmpty.message}")
    public String getDetergentShortName() {
        return detergentShortName;
    }

    public void setDetergentShortName(String detergentShortName) {
        this.detergentShortName = detergentShortName;
    }

    @Column(name = "DETERGENT_NAME")
    @Size(min = 3, max = 80, message = "{validation.detergent.DetergentName.Size.message}")
    @NotEmpty(message = "{validation.detergent.DetergentName.NotEmpty.message}")
    public String getDetergentName() {
        return detergentName;
    }

    public void setDetergentName(String detergentName) {
        this.detergentName = detergentName;
    }

    @Column(name = "IMAGE")
    @Basic(fetch = FetchType.LAZY)
    @Lob
    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    @ManyToMany
    @JoinTable(name = "PURPOSE_DETERGENT_RELATION",
    joinColumns = @JoinColumn(name = "DETERGENT_ID"),
    inverseJoinColumns = @JoinColumn(name = "PURPOSE_ID"))
    public Set<Purpose> getPurposes() {
        return purposes;
    }

    public void setPurposes(Set<Purpose> purposes) {
        this.purposes = purposes;
    }

    @ManyToMany
    @JoinTable(
            name = "DETERGENT_DOSAGE_RELATION",
            joinColumns = @JoinColumn(name = "DETERGENT_ID"),
            inverseJoinColumns = @JoinColumn(name = "DOSAGE_ID")
    )
    public Set<Dosage> getDosages() {
        return dosages;
    }

    public void setDosages(Set<Dosage> dosages) {
        this.dosages = dosages;
    }

    @Transient
    public List<Dosage> getDosagesAsList() {
        return new ArrayList<Dosage>(dosages);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Detergent detergent = (Detergent) o;

        return new EqualsBuilder()
                .append(id, detergent.id)
                .append(detergentShortName, detergent.detergentShortName)
                .append(detergentName, detergent.detergentName)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(detergentShortName)
                .append(detergentName)
                .toHashCode();
    }

    @Override
    public String toString() {
        return "Detergent{" +
                "id=" + id +
                ", detergentShortName='" + detergentShortName + '\'' +
                ", detergentName='" + detergentName + '\'' +
                '}';
    }
}
