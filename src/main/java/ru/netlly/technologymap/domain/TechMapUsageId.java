package ru.netlly.technologymap.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
public class TechMapUsageId implements Serializable{
    private TechnologyMap technologyMap;
    private Usage usage;

    public TechMapUsageId() {
    }

    public TechMapUsageId(TechnologyMap technologyMap, Usage usage) {
        this.technologyMap = technologyMap;
        this.usage = usage;
    }

    @ManyToOne
    public TechnologyMap getTechnologyMap() {
        return technologyMap;
    }

    public void setTechnologyMap(TechnologyMap technologyMap) {
        this.technologyMap = technologyMap;
    }

    @ManyToOne
    public Usage getUsage() {
        return usage;
    }

    public void setUsage(Usage usage) {
        this.usage = usage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        TechMapUsageId that = (TechMapUsageId) o;

        return new EqualsBuilder()
                .append(technologyMap, that.technologyMap)
                .append(usage, that.usage)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(technologyMap)
                .append(usage)
                .toHashCode();
    }

    @Override
    public String toString() {
        return "TechMapUsageId{" +
                "technologyMap=" + technologyMap +
                ", usage=" + usage +
                '}';
    }
}
