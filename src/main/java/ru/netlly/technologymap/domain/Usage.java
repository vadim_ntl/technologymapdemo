package ru.netlly.technologymap.domain;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name = "USAGES")
public class Usage implements Serializable, Convertible{
    public final static String[] standards;

    private Long id;
    private String usageName;
    private String usageStandard;

    static {
        standards = new String[2];
        standards[0] = "Standard";
        standards[1] = "Nonstandard";
    }

    public Usage() {
    }

    public Usage(String usageName, String usageStandard) {
        this.usageName = usageName;
        this.usageStandard = usageStandard;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "USAGE_NAME")
    public String getUsageName() {
        return usageName;
    }

    public void setUsageName(String usageName) {
        this.usageName = usageName;
    }

    @Column(name = "USAGE_STANDARD")
    public String getUsageStandard() {
        return usageStandard;
    }

    public void setUsageStandard(String usageStandard) {
        this.usageStandard = usageStandard;
    }

    @Override
    public String toString() {
        return "Usage{" +
                "id=" + id +
                ", usageName='" + usageName + '\'' +
                ", usageStandard='" + usageStandard + '\'' +
                '}';
    }
}
