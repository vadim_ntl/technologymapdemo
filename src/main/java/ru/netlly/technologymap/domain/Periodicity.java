package ru.netlly.technologymap.domain;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "PERIODICITY")
public class Periodicity implements Serializable, Convertible{
    private Long id;
    private String periodicityName;

    public Periodicity() {
    }

    public Periodicity(String periodicityName) {
        this.periodicityName = periodicityName;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "PERIODICITY_NAME")
    @Size(min = 3, max = 50, message = "{validation.periodicity.PeriodicityName.Size.message}")
    @NotEmpty(message = "{validation.periodicity.PeriodicityName.NotEmpty.message}")
    public String getPeriodicityName() {
        return periodicityName;
    }

    public void setPeriodicityName(String periodicityName) {
        this.periodicityName = periodicityName;
    }

    @Override
    public String toString() {
        return "Periodicity{" +
                "id=" + id +
                ", periodicityName='" + periodicityName + '\'' +
                '}';
    }
}
