package ru.netlly.technologymap.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name = "DOSAGE")
public class Dosage implements Serializable, Convertible{
    private Long id;
    private String dosageContent;
    private byte[] image;

    public Dosage() {
    }

    public Dosage(String dosageContent) {
        this.dosageContent = dosageContent;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "DOSAGE_CONTENT")
    public String getDosageContent() {
        return dosageContent;
    }

    public void setDosageContent(String dosageContent) {
        this.dosageContent = dosageContent;
    }

    @Column(name = "IMAGE")
    @Basic(fetch = FetchType.LAZY)
    @Lob
    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String toString() {
        return "Dosage{" +
                "id=" + id +
                ", dosageContent='" + dosageContent + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Dosage dosage = (Dosage) o;

        return new EqualsBuilder()
                .append(id, dosage.id)
                .append(dosageContent, dosage.dosageContent)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(dosageContent)
                .toHashCode();
    }
}
