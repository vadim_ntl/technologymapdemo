package ru.netlly.technologymap.domain;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "TEMPERATURE")
public class Temperature implements Serializable, Convertible{
    private Long id;
    private String temperatureValue;

    public Temperature() {
    }

    public Temperature(String temperatureValue) {
        this.temperatureValue = temperatureValue;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "TEMPERATURE_VALUE")
    @Size(min = 1, max = 10, message = "{validation.temperature.TemperatureValue.Size.message}")
    @NotEmpty(message = "{validation.temperature.TemperatureValue.NotEmpty.message}")
    public String getTemperatureValue() {
        return temperatureValue;
    }

    public void setTemperatureValue(String temperatureValue) {
        this.temperatureValue = temperatureValue;
    }

    @Override
    public String toString() {
        return "Temperature{" +
                "id=" + id +
                ", value='" + temperatureValue + '\'' +
                '}';
    }
}

