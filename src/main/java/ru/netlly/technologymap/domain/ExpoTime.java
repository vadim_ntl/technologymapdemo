package ru.netlly.technologymap.domain;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "EXPOTIME")
public class ExpoTime implements Serializable, Convertible{
    private Long id;
    private String expoTimeValue;

    public ExpoTime() {
    }

    public ExpoTime(String expoTimeValue) {
        this.expoTimeValue = expoTimeValue;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "EXPOTIME_VALUE")
    @Size(min = 1, max = 50, message = "{validation.expotime.ExpoTimeValue.Size.message}")
    @NotEmpty(message = "{validation.expotime.ExpoTimeValue.NotEmpty.message}")
    public String getExpoTimeValue() {
        return expoTimeValue;
    }

    public void setExpoTimeValue(String expoTimeValue) {
        this.expoTimeValue = expoTimeValue;
    }

    @Override
    public String toString() {
        return "ExpoTime{" +
                "id=" + id +
                ", expoTimeValue='" + expoTimeValue + '\'' +
                '}';
    }
}
