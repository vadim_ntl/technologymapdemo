package ru.netlly.technologymap.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "TECHNOLOGYMAP")
public class TechnologyMap implements Serializable, Convertible{
    private Long id;
    private AreaColor areaColor1;
    private AreaColor areaColor2;
    private Area area;
    private Purpose purpose;
    private Detergent detergent;
    private Dosage dosage;
    private Set<TechMapUsage> techMapUsages =
            new HashSet<TechMapUsage>();
    private Periodicity periodicity;
    private Temperature temperature;
    private ExpoTime expoTime;
    private Siz siz;

    public TechnologyMap() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @OneToOne
    @JoinColumn(name = "AREACOLOR1_ID")
    public AreaColor getAreaColor1() {
        return areaColor1;
    }

    public void setAreaColor1(AreaColor areaColor1) {
        this.areaColor1 = areaColor1;
    }

    @OneToOne
    @JoinColumn(name = "AREACOLOR2_ID")
    public AreaColor getAreaColor2() {
        return areaColor2;
    }

    public void setAreaColor2(AreaColor areaColor2) {
        this.areaColor2 = areaColor2;
    }

    @OneToOne
    @JoinColumn(name = "AREA_ID")
    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    @OneToOne
    @JoinColumn(name = "PURPOSE_ID")
    public Purpose getPurpose() {
        return purpose;
    }

    public void setPurpose(Purpose purpose) {
        this.purpose = purpose;
    }

    @OneToOne
    @JoinColumn(name = "DETERGENT_ID")
    public Detergent getDetergent() {
        return detergent;
    }

    public void setDetergent(Detergent detergent) {
        this.detergent = detergent;
    }

    @OneToOne
    @JoinColumn(name = "DOSAGE_ID")
    public Dosage getDosage() {
        return dosage;
    }

    public void setDosage(Dosage dosage) {
        this.dosage = dosage;
    }

    @OneToMany(mappedBy = "pk.technologyMap", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @OrderBy("ORDERNO")
    public Set<TechMapUsage> getTechMapUsages() {
        return techMapUsages;
    }

    public void setTechMapUsages(Set<TechMapUsage> techMapUsages) {
        this.techMapUsages = techMapUsages;
    }

    @Transient
    public String getUsagesAsString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (TechMapUsage techMapUsage : getTechMapUsages()) {
            if (stringBuilder.length() == 0) {
                stringBuilder.append(techMapUsage.getOrderNo());
                stringBuilder.append(". ");
                stringBuilder.append(techMapUsage.getUsage().getUsageName());
            } else {
                stringBuilder.append("<br/>");
                stringBuilder.append(techMapUsage.getOrderNo());
                stringBuilder.append(". ");
                stringBuilder.append(techMapUsage.getUsage().getUsageName());
            }
        }
        return stringBuilder.toString();
    }

    @OneToOne
    @JoinColumn(name = "PERIODICITY_ID")
    public Periodicity getPeriodicity() {
        return periodicity;
    }

    public void setPeriodicity(Periodicity periodicity) {
        this.periodicity = periodicity;
    }

    @OneToOne
    @JoinColumn(name = "TEMPERATURE_ID")
    public Temperature getTemperature() {
        return temperature;
    }

    public void setTemperature(Temperature temperature) {
        this.temperature = temperature;
    }

    @OneToOne
    @JoinColumn(name = "EXPOTIME_ID")
    public ExpoTime getExpoTime() {
        return expoTime;
    }

    public void setExpoTime(ExpoTime expoTime) {
        this.expoTime = expoTime;
    }

    @OneToOne
    @JoinColumn(name = "SIZ_ID")
    public Siz getSiz() {
        return siz;
    }

    public void setSiz(Siz siz) {
        this.siz = siz;
    }

    @Override
    public String toString() {
        return "TechnologyMap{" +
                "id=" + id +
                ", areaColor1=" + areaColor1 +
                ", areaColor2=" + areaColor2 +
                ", area=" + area +
                ", purpose=" + purpose +
                ", detergent=" + detergent +
                ", dosage=" + dosage +
                ", techMapUsages=" + techMapUsages +
                ", periodicity=" + periodicity +
                ", temperature=" + temperature +
                ", expoTime=" + expoTime +
                ", siz=" + siz +
                '}';
    }
}
