package ru.netlly.technologymap.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "PURPOSE")
public class Purpose implements Serializable, Convertible{
    private Long id;
    private String purposeName;
    private Set<Detergent> detergents = new HashSet<Detergent>();

    public Purpose() {
    }

    public Purpose(String purposeName) {
        this.purposeName = purposeName;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "PURPOSE_NAME")
    public String getPurposeName() {
        return purposeName;
    }

    public void setPurposeName(String purposeName) {
        this.purposeName = purposeName;
    }

    @ManyToMany
    @JoinTable(name = "PURPOSE_DETERGENT_RELATION",
            joinColumns = @JoinColumn(name = "PURPOSE_ID"),
            inverseJoinColumns = @JoinColumn(name = "DETERGENT_ID"))
    public Set<Detergent> getDetergents() {
        return detergents;
    }

    public void setDetergents(Set<Detergent> detergents) {
        this.detergents = detergents;
    }

    @Transient
    public List<Detergent> getDetergentsAsList() {
        return new ArrayList<Detergent>(detergents);
    }

    public String toString() {
        return "Purpose{" +
                "id=" + id +
                ", purposeName='" + purposeName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Purpose purpose = (Purpose) o;

        return new EqualsBuilder()
                .append(id, purpose.id)
                .append(purposeName, purpose.purposeName)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(purposeName)
                .toHashCode();
    }
}
